package org.aestia.exchange;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.apache.mina.core.session.IoSession;

import java.io.IOException;
import java.net.SocketAddress;
import java.net.InetSocketAddress;

import org.aestia.kernel.Main;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.apache.mina.transport.socket.SocketAcceptor;

@Slf4j
public class ExchangeServer {
    private SocketAcceptor acceptor;

    public ExchangeServer() {
        this.acceptor = new NioSocketAcceptor();
        this.acceptor.setReuseAddress(true);
        this.acceptor.setHandler(new ExchangeHandler());
    }

    public void start() {
        if (this.acceptor.isActive()) {
            return;
        }
        try {
            this.acceptor.bind(new InetSocketAddress(Main.config.getExchangeIp(), Main.config.getExchangePort()));
            log.info("Server started on port {} and ip {}", Main.config.getExchangePort(), Main.config.getExchangeIp());
        } catch (IOException e) {
            log.error("Fail to bind acceptor : {}" + e.getMessage());
        }
    }

    public void stop() {
        if (!this.acceptor.isActive()) {
            return;
        }
        this.acceptor.unbind();
        for (final IoSession session : this.acceptor.getManagedSessions().values()) {
            if (session.isConnected() || !session.isClosing()) {
                session.close(true);
            }
        }
        this.acceptor.dispose();
        log.info("Exchange server stoped");
    }

    public Map<Long, ExchangeClient> getClients() {
        final Map<Long, ExchangeClient> clients = new HashMap<>();
        try {
            for (final Map.Entry<Long, IoSession> entry : this.acceptor.getManagedSessions().entrySet()) {
                final Long id = entry.getKey();
                final IoSession session = entry.getValue();
                if (session == null) {
                    continue;
                }
                if (!(session.getAttribute("client") instanceof ExchangeClient)) {
                    continue;
                }
                final ExchangeClient client = (ExchangeClient) session.getAttribute("client");
                if (client == null) {
                    continue;
                }
                clients.put(id, client);
            }
        } catch (Exception ignored) {
        }
        return clients;
    }
}
