package org.aestia.kernel;

import lombok.extern.slf4j.Slf4j;
import org.aestia.login.LoginServer;
import org.aestia.exchange.ExchangeServer;

import java.util.HashMap;
import java.util.Map;

import org.aestia.database.Database;

@Slf4j
public class Main {
    public static Database database = new Database();
    public static Config config = new Config();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(Main::exit));
    }

    public static void main(final String[] arg) {
        start();
    }

    private static void start() {
        final Console console = new Console();
        Main.config.initialize();
        log.info(EmulatorInfos.HARD_NAME.toString());

        if (!Main.database.initializeConnection()) {
            log.error("> Identifiants de connexion invalides");
            log.error("> Redemarrage...");
            System.exit(1);
        }

        Main.database.getServerData().load(null);
        Main.config.setExchangeServer(new ExchangeServer());
        Main.config.getExchangeServer().start();
        Main.config.setLoginServer(new LoginServer());
        Main.config.getLoginServer().start();
        log.info(" > Lancement du serveur termine : " + (System.currentTimeMillis() - Main.config.startTime) + " ms");
        Main.config.setRunning(true);
        console.initialize();
    }

    private static void exit() {
        log.info(" <> Fermeture du jeu <>");
        if (Main.config.isRunning()) {
            Main.config.setRunning(false);
            Main.config.getLoginServer().stop();
            Main.config.getExchangeServer().stop();
        }
        log.info("Le login est a present ferme!");
    }
}
